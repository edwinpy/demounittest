package com.bbva.service.demounittest.business;

import com.bbva.service.demounittest.business.dto.DTOIntCustomer;

import java.util.List;

public interface ICustomerBusiness {

    List<DTOIntCustomer> listAllCustomers();
}
