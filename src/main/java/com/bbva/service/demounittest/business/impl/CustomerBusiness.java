package com.bbva.service.demounittest.business.impl;

import com.bbva.service.demounittest.business.ICustomerBusiness;
import com.bbva.service.demounittest.business.dto.DTOIntCustomer;
import com.bbva.service.demounittest.dao.ICustomerDAO;
import com.bbva.service.demounittest.dao.data.model.CustomerEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomerBusiness implements ICustomerBusiness {

    @Autowired
    private ICustomerDAO customerDAO;

    @Override
    public List<DTOIntCustomer> listAllCustomers() {

        List<CustomerEntity> lista = customerDAO.findAll();
        List<DTOIntCustomer> listOut = new ArrayList<>();
        for(CustomerEntity customer : lista){
            DTOIntCustomer dtoIntCustomer = new DTOIntCustomer();
            dtoIntCustomer.setLastName(customer.getLastName());
            dtoIntCustomer.setFirstName(customer.getFirstName());
            dtoIntCustomer.setId(customer.getId());
            dtoIntCustomer.setSecondLastName(customer.getSecondLastName());
            listOut.add(dtoIntCustomer);
        }
        return listOut;
    }
}
