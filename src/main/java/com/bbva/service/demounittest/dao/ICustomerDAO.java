package com.bbva.service.demounittest.dao;

import com.bbva.service.demounittest.dao.data.model.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICustomerDAO extends JpaRepository<CustomerEntity, Integer> {
}
