package com.bbva.service.demounittest.facade;

import com.bbva.service.demounittest.canonic.Customer;

import java.util.HashMap;
import java.util.List;

public interface ICustomerService {

    List<Customer> listAllCustomers();
}
