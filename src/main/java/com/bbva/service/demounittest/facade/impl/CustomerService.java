package com.bbva.service.demounittest.facade.impl;

import com.bbva.service.demounittest.business.ICustomerBusiness;
import com.bbva.service.demounittest.canonic.Customer;
import com.bbva.service.demounittest.facade.ICustomerService;
import com.bbva.service.demounittest.facade.mapper.ListAllCustomerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
public class CustomerService implements ICustomerService {

    @Autowired
    private ICustomerBusiness customerBusiness;

    @Autowired
    private ListAllCustomerMapper listAllCustomerMapper;


    @Override
    @GetMapping("/listAllCustomer")
    public List<Customer> listAllCustomers() {
        List<Customer> customerList = listAllCustomerMapper.mapOut(customerBusiness.listAllCustomers());
        return customerList;
    }
}
