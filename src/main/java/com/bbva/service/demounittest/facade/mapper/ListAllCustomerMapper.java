package com.bbva.service.demounittest.facade.mapper;

import com.bbva.service.demounittest.business.dto.DTOIntCustomer;
import com.bbva.service.demounittest.canonic.Customer;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ListAllCustomerMapper {

    public List<Customer> mapOut (List<DTOIntCustomer> list){
        List<Customer> customerList = new ArrayList<>();
        for(DTOIntCustomer dtoIntCustomer : list){
            Customer customer = new Customer();
            customer.setFirstName(dtoIntCustomer.getFirstName());
            customer.setId(dtoIntCustomer.getId());
            customer.setLastName(dtoIntCustomer.getLastName());
            customer.setSecondLastName(dtoIntCustomer.getSecondLastName());

            customerList.add(customer);
        }
        return customerList;
    }
}
